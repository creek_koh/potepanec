require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  describe "カテゴリーページに関連するテスト" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon, name: "Example", parent: taxonomy.root, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }

    background do
      visit potepan_category_path(taxon.id)
    end

    it "カテゴリページで表示されている内容" do
      expect(page).to have_content "商品カテゴリー"
      expect(page).to have_content "色から探す"
      expect(page).to have_content "サイズから探す"
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end

    context "サイドバーのカテゴリーをクリックした時" do
      it "正しいカテゴリーページへ遷移する" do
        click_link "Example"
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    context "商品をクリックした時" do
      it "プロダクト詳細ページへ遷移して、カテゴリー一覧ページへ戻る" do
        click_link product.name
        expect(current_path).to eq potepan_product_path(product.id)
        click_link "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(product.taxons.first.id)
      end
    end
  end
end

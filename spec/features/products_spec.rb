require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:unrelated_product) { create(:product, taxons: [other_taxon]) }
  let!(:other_taxon) { create(:taxon) }

  background do
    visit potepan_product_path(product.id)
  end

  context "関連商品の表示数テスト" do
    let!(:related_product) { create_list(:product, 5, taxons: [taxon]) }

    it "関連商品4つ表示" do
      expect(page).to have_selector ".productBox", count: 4
    end
  end

  it "関連商品を表示" do
    within(".productBox") do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
    end
  end

  it "関連商品をクリックすると、その商品の詳細ページを表示する" do
    within(".productBox") do
      click_on related_product.name
      expect(page).to have_current_path(potepan_product_path(related_product.id))
    end
  end

  it "関連商品をクリックすると、その商品詳細ページを表示する" do
    within(".productBox") do
      click_on related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
    end
  end

  it "関連しない商品は表示しない" do
    within(".productBox") do
      expect(page).not_to have_content unrelated_product.name
    end
  end
end

require "rails_helper"

RSpec.describe "Potepan::ProductsController", type: :request do
  describe "#show" do
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "レスポンスが成功すること" do
      expect(response).to be_successful
    end

    it "レスポンスの結果が200であること" do
      expect(response).to have_http_status 200
    end

    it "商品名が含まれている" do
      expect(response.body).to include product.name
    end

    it "値段が含まれている" do
      expect(response.body).to include product.display_price.to_s
    end

    it '商品説明が含まれている' do
      expect(response.body).to include product.description
    end

    it "@related_productsは、関連商品商品を４つもつ" do
      expect(controller.instance_variable_get("@related_products").size).to eq 4
    end
  end
end

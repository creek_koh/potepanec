require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /potepan/categories/show" do
    let(:taxon) { create(:taxon) }
    let(:taxonomy) { create(:taxonomy) }
    let(:product) { create(:product) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "レスポンスが成功すること" do
      expect(response).to be_successful
    end

    it "taxonが表示されていること" do
      expect(response.body).to include taxon.name
    end

    it "taxonomyが表示されていること" do
      expect(response.body).to include taxonomy.name
    end

    it "商品の価格、名前、画像が表示されていること" do
      taxon.products.each do |product|
        expect(response.body).to include product.name
        expect(response.body).to include product.display_price
        expect(response.body).to include product.images
      end
    end
  end
end

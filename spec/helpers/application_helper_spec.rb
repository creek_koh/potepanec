require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title(page_title)" do
    it "ページタイトルが文字列を含むとき" do
      expect(full_title("test")).to eq "test - BIG BAG Store"
    end

    it "ページタイトルがnilだったとき" do
      expect(full_title(nil)).to eq "BIG BAG Store"
    end

    it "ページタイトルが空だったとき" do
      expect(full_title("")).to eq "BIG BAG Store"
    end
  end
end
